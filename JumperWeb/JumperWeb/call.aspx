﻿<%@ Page Language="C#" Inherits="JumperWeb.call" CodeBehind="~/call.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="head" Src="components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header" Src="components/header.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="components/footer.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Jumper Call</title>
        <meta name="robots" content="noindex, nofollow" />
        <meta name="description" content="Jumper Call">
        <meta itemprop="name" content="Jumper Call">
        <meta itemprop="description" content="Jumper Call">
        <meta name="twitter:title" content="Jumper Call">
        <meta name="twitter:description" content="Jumper Call">
        <meta name="og:title" content="Jumper Call">
        <meta name="og:description" content="Jumper Call">
        <meta itemprop="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="twitter:image:src" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="og:image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">            
        <script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/call.js"></script>
</head>
<body>
	<form id="form1" runat="server">
        <div ID="IsMobileDeviceFlag" runat="server" class=""></div>

        <uc:header id="header1" runat="server"></uc:header>
                
        <div class="section-atf section-white-pure">
                <div class="container">
                    <div class="row row-cover row-atf row-flex row-flex-center">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <i class="color-primary fa fa-4x fa-mobile"></i>
                                <h3>Please open this link on your phone after downloading the Jumper app from the App Store</h3>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</form>
</body>
</html>
