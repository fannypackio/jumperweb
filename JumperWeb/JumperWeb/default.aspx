﻿<%@ Page Language="C#" Inherits="JumperWeb.Default" %>
<%@ Register TagPrefix="uc" TagName="head" Src="components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header" Src="components/header.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="components/footer.ascx" %>
<!DOCTYPE html>
<html class="no-js" lang="">
    <head runat="server">
        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Jumper</title>
        <meta name="description" content="Where people and places meet">
        <meta itemprop="name" content="Jumper">
        <meta itemprop="description" content="Where people and places meet">
        <meta name="twitter:title" content="Jumper">
        <meta name="twitter:description" content="Where people and places meet">
        <meta name="og:title" content="Jumper">
        <meta name="og:description" content="Where people and places meet">  
        <meta itemprop="image" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="image" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="twitter:image:src" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="og:image" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">        
        <script>
            $(function(){

              $("#TextAppLinkTo").mask("(999) 999-9999");

              $("#TextAppLinkTo").on("blur", function() {
                  var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

                  if( last.length == 5 ) {
                      var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

                      var lastfour = last.substr(1,4);

                      var first = $(this).val().substr( 0, 9 );

                      $(this).val( first + move + '-' + lastfour );
                  }
              });
            });
        </script>
    
    </head>
    <body class="hide-h6">
    	<form id="form1" runat="server">
    		<!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <!-- global components -->

            <uc:overlay id="overlay1" runat="server"></uc:overlay>
            <uc:banner id="banner1" runat="server"></uc:banner>
            <uc:header id="header1" runat="server"></uc:header>

            <!-- for main homepage -->
            <div runat="server" id="smsCheck" style="display: none;"></div>
            <div class="section-atf section-gradient">
                <div class="container">
                    <div class="row row-cover row-atf row-flex row-flex-center">
                        <div class="col-md-5 col-md-push-7">
                            <div class="component text-right">
                                <h1 class="mb-40 mobile-only text-left">Where people and places meet</h1>
                                <div class="iphone-wrap">
                                    <img class="iphone" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/atf-customer.png" alt="">
                                    <img class="iphone" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/atf-partner.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-md-pull-5">
                            <div class="component text-left">
                                <h1 class="desktop-only">Where people and places meet</h1>
                            </div>
                            <div class="row mt-40 mt-20-mobile">
                                <div class="col-md-6">
                                    <div class="min-ht-100">
                                        <h2>Customers</h2>
                                        <p>Video call and shop at any business from wherever you are</p>
                                        <a class="link-white vis-hidden desktop-only" href="/partner">More Info & Features</a>
                                        <a href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488" class="mt-20 mb-40 cta cta-white">Get Customer App</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="min-ht-100">
                                        <h2>Businesses</h2>
                                        <p>Increase sales and engagement with customer-facing video calls</p>
                                        <a class="link-white" href="/partner">More Info & Features</a>
                                        <a href="https://itunes.apple.com/us/app/jumper-for-partners/id1418965492" class="mt-20 mb-40 cta cta-white">Get Partner App</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SectionFeatures" class="section-features section-white-pure">
                <img class="iphone-bg" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/iphone-biz-details.png"/>
                <img class="iphone-bg iphone-bg-2" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/iphone-biz-list.png"/>
                <div class="container">
                    <div class="row row-cover row-features row-flex row-flex-center">
                        <div class="inner-flex-wrap">
                            <div class="col-md-6 col-md-push-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="component text-left">
                                            <h3>What is Jumper?</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="component component-v-spacing text-left">
                                            <h5>A single place to video call any business</h5>
                                            <h6>Search by category or location</h6>
                                        </div>
                                        <div class="component component-v-spacing text-left">
                                            <h5>Browse and view business details</h5>
                                            <h6>One touch away from "being there"</h6>
                                        </div>
                                        <div class="component component-v-spacing text-left">
                                            <h5>Tap to start a video call</h5>
                                            <h6>You will be connected with a representative at that business</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-pull-5">
                                <img class="feature-image-item-alt" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-auto.png" alt="">                                            
                                <img class="feature-image-item-alt" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-hotel.png" alt="">
                                <img class="feature-image-item-alt" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-lofts.png" alt="">                                
                            </div>
                            <div class="col-md-3 col-md-pull-5">
                                <div class="feature-image-wrapper hidden-img-2">
                                    <div class="feature-image-wrap hide feature-mobile-hide feature-image-wrap-2">
                                        <img class="feature-image-blur" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-auto-details.png" alt="">
                                    </div>
                                </div>
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-download section-white-pure">
                <div class="container">
                    <div class="row row-cover row-features row-flex row-flex-center">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <h3>Where will you go today?</h3>
                            </div>                      
                            <div class="component component-v-spacing text-center">
                                <h4 class="mb-20">Available For iOS</h4>
                                <span class="inline-70">
                                    <h5 class="mt-20 mb-20">Customer App</h5>
                                    <a href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488">
                                        <img class="mb-40 store-img img-small" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/app-store-image.png" />
                                    </a>
                                </span>
                                <span class="inline-70">
                                    <h5 class="mt-20 mb-20">Partner App</h5>                                                                
                                    <a href="https://itunes.apple.com/us/app/jumper-for-partners/id1418965492">
                                        <img class="mb-40 store-img img-small" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/app-store-image.png" />
                                    </a>                                
                                </span>
                            </div>
                            <div class="component component-v-spacing text-center">
                                <span class="inline-70">
                                    <h4 class="mb-20">Coming Soon For Android & Web</h4>
                                    <img class="store-img store-img-coming-soon img-small" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/android-coming-soon.png" />
                                </span>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

            <!-- for homepage footer -->

            <uc:footer id="footer1" runat="server"></uc:footer>

            <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
            <script>
                (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                e.src='//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
                ga('create','UA-XXXXX-X','auto');ga('send','pageview');
            </script>

    	</form>
</body>
</html>
