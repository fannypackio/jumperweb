﻿<%@ Page Language="C#" Inherits="JumperWeb.default_partner" %>
<%@ Register TagPrefix="uc" TagName="head" Src="../components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="../components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="../components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header_partner" Src="../components/header_partner.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="../components/footer.ascx" %>
<!DOCTYPE html>
<html>
    <head runat="server">
        <script>
          window.intercomSettings = {
            app_id: "pc4unkz1"
          };
        </script>
        <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/pc4unkz1';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Become A Jumper Partner</title>
        <meta name="description" content="Video call with your customers">
        <meta itemprop="name" content="Become A Jumper Partner">
        <meta itemprop="description" content="Video call with your customers">
        <meta name="twitter:title" content="Become A Jumper Partner">
        <meta name="twitter:description" content="Video call with your customers">
        <meta name="og:title" content="Become A Jumper Partner">
        <meta name="og:description" content="Video call with your customers">
        <meta itemprop="image" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="image" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="twitter:image:src" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="og:image" content="https:<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <script>
            $(function(){

              $("#TextAppLinkTo").mask("(999) 999-9999");

              $("#TextAppLinkTo").on("blur", function() {
                  var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

                  if( last.length == 5 ) {
                      var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

                      var lastfour = last.substr(1,4);

                      var first = $(this).val().substr( 0, 9 );

                      $(this).val( first + move + '-' + lastfour );
                  }
              });
            });
        </script>
    </head>
    <body class="hide-h6">
    	<form id="form1" runat="server">

            <!-- global components -->

            <uc:overlay id="overlay1" runat="server"></uc:overlay>
            <uc:banner id="banner1" runat="server"></uc:banner>
            <uc:header_partner id="header_partner1" runat="server"></uc:header_partner>

            <div runat="server" id="smsCheck" style="display: none;"></div>

            <div class="section-atf-partner section-atf section-gradient">
                <div class="container">
                    <div class="row row-cover row-atf row-flex row-flex-center">
                        <div class="col-md-7 col-md-push-5">
                            <div class="component text-left text-left-sm">
                                <h1>Increase sales and engagement with customer-facing video calls</h1>
                                <h2>The video calling platform designed for you and your customers</h2>
                                <a href="https://itunes.apple.com/us/app/jumper-for-partners/id1418965492" class="mt-20 mb-40 cta cta-white">Get Partner App</a>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-pull-7">
                            <div class="component text-left">
                                <div class="iphone-wrap">
                                    <img class="iphone" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/atf-partner.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SectionFeatures" class="section-features-partner section-white-pure">
                <img class="iphone-bg iphone-bg-2" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/iphone-p-call-history.png"/>                                
                <img class="iphone-bg" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/iphone-lead.png"/>                                
                <div class="container">
                    <div class="row row-cover row-features row-flex row-flex-center">
                        <div class="inner-flex-wrap">
                            <div class="col-md-6 col-md-push-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="component text-left">
                                            <h3>What is Jumper?</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="component component-v-spacing text-left">
                                            <h5>A single place to manage video calls with your customers</h5>
                                            <h6>Make your communication personal by letting your customers video call your business</h6>
                                        </div>
                                        <div class="component component-v-spacing text-left">
                                            <h5>Browse and view customer details</h5>
                                            <h6>One touch away from seeing your customers remotely</h6>
                                        </div>
                                        <div class="component component-v-spacing text-left">
                                            <h5>Tap to start a video call</h5>
                                            <h6>You will be connected with your customer immediately</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-pull-5">
                                <img class="feature-image-item-alt" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-lead-1.png" alt="">                                            
                                <img class="feature-image-item-alt" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-lead-2.png" alt="">
                                <img class="feature-image-item-alt" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-lead-3.png" alt="">                                
                            </div>
                            <div class="col-md-3 col-md-pull-5">
                                <div class="feature-image-wrapper">
<!--                                    <div class="feature-image-wrap feature-image-wrap-2">
                                        <img class="feature-image-blur" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-lead-details.png" alt="">
                                    </div>
-->                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-team section-white-pure">
                <div class="container">
                    <div class="row row-cover row-flex row-flex-center">
                        <div class="inner-flex-wrap">
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="component text-left">
                                            <h3>Invite and manage your team</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="component component-v-spacing text-left">
                                            <h5>A single place to manage your team's video calls with customers</h5>
                                            <h6>No more disconnected, unmanaged video calling apps</h6>
                                        </div>
                                        <div class="component component-v-spacing text-left">
                                            <h5>Add any team members you want to receive video calls</h5>
                                            <h6>Include your whole team, or choose which team members</h6>
                                        </div>
                                        <div class="component component-v-spacing text-left">
                                            <h5>When customers call, we'll ring your team's phones</h5>
                                            <h6>Video call any lead back and keep your communication personal</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-push-1">
                                <div class="team-image-wrapper">
                                    <div class="team-image-wrap">
                                        <img class="team-image" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/tm1.png" />
                                    </div>
                                    <div class="team-image-wrap">
                                        <img class="team-image" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/tm3.png" />
                                    </div>
                                    <div class="team-image-wrap">
                                        <img class="team-image" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/tm2.png" />
                                    </div>
                                    <div class="team-image-wrap team-image-wide">
                                        <img class="team-image" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-team.png" />
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-manage section-white-pure">
                <div class="container">
                    <div class="row row-cover row-flex row-flex-center">
                        <div class="inner-flex-wrap">
                            <div class="col-md-6 col-md-push-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="component text-left">
                                            <h3>View performance and manage your business</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="component component-v-spacing">
                                            <h5>Monitor performance on your team's dashboard</h5>
                                            <h6>From call statistics to viewing active video calls</h6>
                                        </div>
                                        <div class="component component-v-spacing">
                                            <h5>Manage your business, and business profile</h5>
                                            <h6>Set business hours, and photos</h6>
                                        </div>
                                        <div class="component component-v-spacing">
                                            <h5>View video calls in real-time, as they happen</h5>
                                            <h6>Feature coming soon</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-pull-6">
                                <div class="feature-image-wrapper">
                                    <div class="feature-image-wrap no-v-margin-mobile image-up">
                                        <img class="feature-image-blur no-shadow" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-notifications.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-pull-6">
                                <div class="feature-image-wrapper">
                                    <div class="feature-image-wrap no-v-margin-mobile feature-image-wrap-2">
                                        <img class="feature-image-blur no-shadow" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/card-dashboard.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="section-download-partner section-white-pure">
                <div class="container">
                    <div class="row row-cover row-features row-flex row-flex-center">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <h3>Take your business to the next level today</h3>
                            </div>                      
                            <div class="component component-v-spacing text-center">
                                <h4 class="mb-20">Available For iOS</h4>
                                <span class="inline-70">
                                    <h5 class="mt-20 mb-20">Customer App</h5>
                                    <a href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488">
                                        <img class="mb-40 store-img img-small" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/app-store-image.png" />
                                    </a>
                                </span>
                                <span class="inline-70">
                                    <h5 class="mt-20 mb-20">Partner App</h5>                                                                
                                    <a href="https://itunes.apple.com/us/app/jumper-for-partners/id1418965492">
                                        <img class="mb-40 store-img img-small" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/app-store-image.png" />
                                    </a>                                
                                </span>
                            </div>
                            <div class="component component-v-spacing text-center">
                                <span class="inline-70">
                                    <h4 class="mb-20">Coming Soon For Android & Web</h4>
                                    <img class="store-img store-img-coming-soon img-small" src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/android-coming-soon.png" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- for homepage footer -->

            <uc:footer id="footer1" runat="server"></uc:footer>
            
        </form>
    </body>
</html>
