﻿<%@ Page Language="C#" Inherits="JumperWeb.reset_password_partner" %>
<%@ Register TagPrefix="uc" TagName="head" Src="../components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="../components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="../components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header_nolinks" Src="../components/header_nolinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="../components/footer.ascx" %>
<!DOCTYPE html>
<html>
    <head runat="server">
        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Jumper</title>
        <link rel="shortcut icon" href="../favicon.png">
        <meta name="robots" content="noindex, nofollow" />        
        <meta name="description" content="Enter a new password">
        <meta itemprop="name" content="Jumper">
        <meta itemprop="description" content="Enter a new password">
        <meta name="twitter:title" content="Jumper">
        <meta name="twitter:description" content="Enter a new password">
        <meta name="og:title" content="Jumper">
        <meta name="og:description" content="Enter a new password">
        <meta itemprop="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="twitter:image:src" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="og:image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">      
         <script type="text/javascript">
            window.keys = {};
            keys.partnerapi = "<%=System.Configuration.ConfigurationManager.AppSettings["JumperPartner.API.Url"] %>";
        </script>   
        <script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/reset-password.js"></script>        
    </head>
    <body class="partner">
    	<form id="form1" runat="server" onsubmit="return false;" >

            <!-- global components -->

            <uc:overlay id="overlay1" runat="server"></uc:overlay>
            <uc:banner id="banner1" runat="server"></uc:banner>
            <uc:header_nolinks id="header_nolinks1" runat="server"></uc:header_nolinks>

            <!-- Reset password content -->

            <div class="section-atf section-white">
                <div class="container">
                    <div class="row row-cover row-atf">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="component component-password-titles">
                                <h3>Password Reset</h3>
                                <h4>Enter a new password</h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-4">
                            <div class="component component-passwords block-inputs">
                            <!-- <form accept-charset="UTF-8"> -->
                                <input required class="text-field password-input" id="password1" type="password" name="password1" placeholder="Enter New Password" />
                                <input required class="text-field password-input" id="password2" type="password" name="password2" placeholder="Confirm Password" />
                                <button id="resetPasswordButton" class="cta cta-main" type="submit" value="Submit" />Submit</button>
                                <input id="PartnerPhone" style="display: none;" />
                            <!-- </form> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        
            <!-- for homepage footer -->

            <uc:footer id="footer1" runat="server"></uc:footer>
            
        </form>
    </body>
</html>
