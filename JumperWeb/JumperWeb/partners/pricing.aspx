﻿<%@ Page Language="C#" Inherits="JumperWeb.pricing" %>
<%@ Register TagPrefix="uc" TagName="head" Src="../components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="../components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="../components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header_pricing" Src="../components/header_pricing.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="../components/footer.ascx" %>
<!DOCTYPE html>
<html>
    <head runat="server">
        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Jumper Partner Pricing</title>
        <meta name="description" content="Pricing for Partners">
        <meta itemprop="name" content="Jumper">
        <meta itemprop="description" content="Pricing for Partners">
        <meta name="twitter:title" content="Jumper Partner Pricing">
        <meta name="twitter:description" content="Pricing for Partners">
        <meta name="og:title" content="Jumper Partner Pricing">
        <meta name="og:description" content="Pricing for Partners">
        <meta itemprop="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="twitter:image:src" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">
        <meta name="og:image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta-partner.png">        
    </head>
    <body class="">
    	<form id="form1" runat="server">

            <!-- global components -->

            <uc:overlay id="overlay1" runat="server"></uc:overlay>
            <uc:banner id="banner1" runat="server"></uc:banner>
            <uc:header_pricing id="header_pricing1" runat="server"></uc:header_pricing>>

            <div class="section-atf-pricing section-white">
                <div class="container">
                    <div class="row row-cover row-atf row-flex row-flex-center row-flex-column">
                        <div class="col-md-12">
                            <div class="component component-v-spacing text-center">
                                <h1>Simple Pricing For Every Business</h1>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="flex-height">
                                <div class="component component-plan component-v-spacing text-center">
                                    <h3>Individual</h3>
                                    <h5>Try it for your business!</h5>
                                    <h6 class="includes">Includes 1 admin + unlimited video calling</h6>
                                    <h3 class="price">Free</h3>
                                    <h6 class="term">For individuals</h6>
                                </div>
                                <div class="component component-plan component-v-spacing text-center">
                                    <h3>Additional Team Members</h3>
                                    <h5>Invite your team to join your Partner account</h5>
                                    <h6 class="includes">Includes 1 admin & unlimited team members + unlimited video calling</h6>
                                    <h3 class="price">$19/month</h3>
                                    <h6 class="term">Per additional team member</h6>
                                </div>
                                <div class="component component-plan component-v-spacing text-center">
                                    <h3>Enterprise</h3>
                                    <h5>Includes custom features and discounts</h5>
                                    <h6 class="includes">Includes custom features and discounts</h6>
                                    <h3 class="price">Contact Us</h3>
                                    <div class="margin-tb-10">
                                        <a href="tel:+19494245329" class="term inline-link">Call Us</a> <span>|</span> <a href="mailto:enterprise@hellojumper.com" class="term inline-link">Email Us</a>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- for homepage footer -->

            <uc:footer id="footer1" runat="server"></uc:footer>

        </form>
    </body>
</html>
