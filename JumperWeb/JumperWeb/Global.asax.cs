﻿using System.Web;
using System.Web.Routing;

namespace JumperWeb
{
    public class Global : HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("Home", "home", "~/default.aspx");
            routes.MapPageRoute("ResetPassword", "reset-password", "~/reset_password.aspx");
            routes.MapPageRoute("ConfirmEmail", "confirm-email", "~/confirm_email.aspx");
            routes.MapPageRoute("Privacy", "privacy", "~/privacy.aspx");
            routes.MapPageRoute("Terms", "terms", "~/terms.aspx");
            routes.MapPageRoute("Cookies", "cookies", "~/cookies.aspx");
            //routes.MapPageRoute("Call", "call", "~/call.aspx");
            routes.MapPageRoute("Partners", "partner", "~/partners/default_partner.aspx");
            routes.MapPageRoute("Pricing", "pricing", "~/partners/pricing.aspx");
            routes.MapPageRoute("PartnerResetPassword", "partners/reset-password", "~/partners/reset_password_partner.aspx");
            routes.MapPageRoute("PartnerConfirmEmail", "partners/confirm-email", "~/partners/confirm_email_partner.aspx");
            routes.MapPageRoute("Call", "call/{partnerid}/{userid}", "~/call.aspx", false, new RouteValueDictionary { { "partnerid", "" }, { "userid", "" } });
        }
        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
        }
    }
}
