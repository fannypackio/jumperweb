﻿<%@ Control Language="C#" CodeBehind="header.ascx.cs" Inherits="JumperWeb.components.header" %>

<div class="header">
    <div class="pill-nav">
        <i class="fas fa-2x fa-bars pill-bars"></i>
        <i class="fas fa-2x fa-times pill-close"></i>
    </div>
    <div class="container">
        <div class="row row-flex row-flex-center">
            <div class="col-md-2 col-sm-8 col-xs-8">
                <a class="logo-wrapper" href="/home">
                    <svg class="logo" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="" height="" viewBox="0 0 4000 4000" enable-background="new 0 0 4000 4000" xml:space="preserve">
                        <g>
                            <path d="M1482.472,547.578c-630.168,0-1142.847,512.685-1142.847,1142.848c0,206.372,53.253,425.462,158.276,651.167
                                c82.925,178.205,198.246,361.133,342.758,543.699c244.984,309.488,486.517,508.895,496.687,517.23
                                c39.234,32.174,90.772,49.9,145.126,49.9c54.353,0,105.886-17.729,145.128-49.914c10.161-8.348,251.693-207.74,496.685-517.229
                                c144.512-182.562,259.83-365.49,342.756-543.699c105.025-225.709,158.281-444.785,158.281-651.164
                                C2625.316,1060.261,2112.641,547.578,1482.472,547.578z M1978.219,2735.362c-216.769,274.293-436.909,456.223-439.087,458.029
                                c-14.291,11.705-34.938,18.428-56.668,18.428c-21.725,0-42.374-6.723-56.649-18.428c-8.715-7.146-871.167-723.412-871.167-1477.381
                                c-0.007-511.605,416.214-927.829,927.816-927.829c511.613,0,927.834,416.222,927.834,927.829
                                C2410.289,2106.644,2175.332,2485.954,1978.219,2735.362z"/>
                        </g>
                        <g>
                            <path d="M2517.523,547.578c-630.166,0-1142.845,512.684-1142.845,1142.848c0,206.37,53.253,425.462,158.273,651.167
                                c82.927,178.205,198.244,361.133,342.76,543.699c244.984,309.484,486.517,508.895,496.691,517.23
                                c39.232,32.174,90.77,49.9,145.121,49.9c54.354,0,105.889-17.729,145.131-49.914c10.158-8.35,251.691-207.74,496.684-517.229
                                c144.512-182.562,259.832-365.49,342.756-543.703c105.029-225.705,158.281-444.783,158.281-651.16
                                C3660.371,1060.26,3147.691,547.578,2517.523,547.578z M3013.271,2735.364c-216.771,274.291-436.906,456.221-439.088,458.027
                                c-14.289,11.705-34.936,18.428-56.668,18.428c-21.727,0-42.373-6.723-56.646-18.428c-8.715-7.146-871.167-723.412-871.167-1477.383
                                c-0.005-511.606,416.212-927.827,927.814-927.827c511.611,0,927.836,416.22,927.836,927.827
                                C3445.344,2106.64,3210.387,2485.954,3013.271,2735.364z"/>
                        </g>
                        <defs>
                            <linearGradient gradientTransform="rotate(90)" id="MyGradient">
                                <stop offset="0%"  stop-color="#E7683F"/>
                                <stop offset="100%" stop-color="#E74D56"/>
                            </linearGradient>
                        </defs>
                    </svg>
                    <span class="logo-text">jumper</span>
                </a>
            </div>
            <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="dd-header">
                    <a class="cta link-white no-underline">Download</a>
                    <div class="dd-header-menu">
                        <h5>iOS</h5>                        
                        <a href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488" class="cta link-white no-underline">Get Customer App</a>
                        <a href="https://itunes.apple.com/us/app/jumper-for-partners/id1418965492" class="cta link-white no-underline">Get Partner App</a>
                        <h5>Android & Web</h5>
                        <a class="disabled-link cta link-white no-underline">Coming Soon</a>
                    </div>
                </div>
                <a href="https://partner.hellojumper.com" class="cta link-white no-underline">Partner Login</a>
                <div class="dd-header">
                    <a class="cta link-white no-underline">Resources</a>
                    <div class="dd-header-menu">
                        <h5>Branding</h5>                        
                        <a href="//brand.hellojumper.com" class="cta link-white no-underline">Jumper Brand Resources</a>
                        <h5>Support</h5>                        
                        <a href="//help.hellojumper.com" class="cta link-white no-underline">Help Center</a>
                        <a href="//feedback.hellojumper.com" class="cta link-white no-underline">Give Feedback</a>
                    </div>
                </div>
                <div class="dd-header">
                    <a class="cta link-white no-underline">Product</a>
                    <div class="dd-header-menu">
                        <h5>For Customers</h5>                        
                        <a href="#SectionFeatures" class="cta link-white no-underline">Features</a>
                        <h5>For Businesses</h5>                        
                        <a href="/partner" class="cta link-white no-underline">More Info & Features</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>