﻿<%@ Control Language="C#" CodeBehind="head.ascx.cs" Inherits="JumperWeb.components.head" %>

<!-- General Meta -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="shortcut icon" href="favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Twitter -->

<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@hellojumper">
<meta name="twitter:creator" content="@hellojumper">

<!-- Open Graph general (Facebook, Pinterest & Google+) -->

<meta name="og:url" content="https://hellojumper.com">
<meta name="og:site_name" content="Jumper">
<meta name="og:type" content="website">
<!-- CSS -->
<link rel="stylesheet" href="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/css/main.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
<!-- JS -->
<script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/vendor/bootstrap.min.js"></script>
<script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/lib/jquery.phonemask.js"></script>
<script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/main.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1659117770971488');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1659117770971488&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121648438-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121648438-2');
</script>
