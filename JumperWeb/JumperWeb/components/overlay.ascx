﻿<%@ Control Language="C#" CodeBehind="overlay.ascx.cs" Inherits="JumperWeb.components.overlay" %>
<div class="overlay text-center">
    <div class="container">
        <div class="row row-cover row-features row-flex row-flex-center">
            <div class="inner-flex-wrap">
                <div class="col-md-12">
                    <div class="component text-center component-v-spacing">
                        <h3>Are you sure you want to remove this team member?</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <button class="cta">Cancel</button>
                </div>
                <div class="col-md-6">
                    <button class="cta cta-main">Confirm</button>
                </div>
            </div>
        </div>
    </div>
</div>