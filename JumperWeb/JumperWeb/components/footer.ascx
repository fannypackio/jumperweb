﻿<%@ Control Language="C#" Inherits="JumperWeb.components.footer" %>
<div class="footer section-gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="component">
                    <h6>© Jumper Inc. All Rights Reserved.</h6>
                </div>
            </div>
            <div class="col-md-4">
                <div class="component">
                    <a href="//hellojumper.com">Jumper Home</a>
                    <a href="//hellojumper.com/partner">Jumper for Business</a>
                    <a href="//brand.hellojumper.com">Jumper Brand Resources</a>                    
                    <a href="//hellojumper.com/privacy">Privacy Policy</a>
                    <a href="//hellojumper.com/terms">Terms of Service</a>
                    <a href="//hellojumper.com/cookies">Cookie Policy</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="component">
                    <a href="mailto:team@hellojumper.com">Contact</a>
                    <a href="//facebook.com/hellojumper" target="_blank">Facebook</a>
                    <a href="//twitter.com/hellojumper" target="_blank">Twitter</a>                    
                    <a href="//help.hellojumper.com" target="_blank">Help Center</a>
                    <a href="//feedback.hellojumper.com" target="_blank">Give Feedback</a>
                </div>
            </div>
        </div>
    </div>
</div>