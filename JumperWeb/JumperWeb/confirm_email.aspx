﻿<%@ Page Language="C#" Inherits="JumperWeb.confirm_email" %>
<%@ Register TagPrefix="uc" TagName="head" Src="components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header_nolinks" Src="components/header_nolinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="components/footer.ascx" %>
<!DOCTYPE html>
<html>
    <head runat="server">
        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Jumper</title>
        <meta name="robots" content="noindex, nofollow" />        
        <meta name="description" content="Confirm your email">
        <meta itemprop="name" content="Jumper">
        <meta itemprop="description" content="Confirm your email">
        <meta name="twitter:title" content="Jumper">
        <meta name="twitter:description" content="Confirm your email">
        <meta name="og:title" content="Jumper">
        <meta name="og:description" content="Confirm your email">
        <meta itemprop="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="twitter:image:src" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="og:image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <script type="text/javascript">
            window.keys = {};
            keys.customerapi = "<%=System.Configuration.ConfigurationManager.AppSettings["JumperCustomer.API.Url"] %>";
        </script>            
        <script src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/js/confirm-email.js"></script>        
    </head>
    <body>
    	<form id="form1" runat="server">
            <!-- global components -->

            <uc:overlay id="overlay1" runat="server"></uc:overlay>
            <uc:banner id="banner1" runat="server"></uc:banner>
            <uc:header_nolinks id="header_nolinks1" runat="server"></uc:header_nolinks>            

            <!-- Confirm email content -->

            <div class="section-atf section-white">
                <div class="container">
                    <div class="row row-cover row-atf">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="component text-center component-confirmed-email">
                                <h3>Your email address is confirmed!</h3>
                                <h4>Open Jumper to get started!</h4>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-4">
                            <div class="component text-center component-confirmed-email">
                                <!-- if partner, change url to jumperpartner:// -->
                                <a href="jumperapp://" class="cta cta-main cta-block center-block">Open App</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <!-- for homepage footer -->

            <uc:footer id="footer1" runat="server"></uc:footer>
            
        </form>
    </body>
</html>
