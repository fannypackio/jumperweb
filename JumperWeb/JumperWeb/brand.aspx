﻿<%@ Page Language="C#" Inherits="JumperWeb.brand" %>
<%@ Register TagPrefix="uc" TagName="head" Src="components/head.ascx" %>
<%@ Register TagPrefix="uc" TagName="banner" Src="components/banner.ascx" %>
<%@ Register TagPrefix="uc" TagName="overlay" Src="components/overlay.ascx" %>
<%@ Register TagPrefix="uc" TagName="header" Src="components/header_nolinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="footer" Src="components/footer.ascx" %>
<!DOCTYPE html>
<html class="no-js">
    <head runat="server">
        <uc:head id="head1" runat="server"></uc:head>
        <!-- dynamic -->
        <title>Jumper Brand Resources</title>
        <meta name="description" content="Jumper brand resources and styleguide">
        <meta itemprop="name" content="Jumper Brand Resources">
        <meta itemprop="description" content="Jumper brand resources and styleguide">
        <meta name="twitter:title" content="Jumper Brand Resources">
        <meta name="twitter:description" content="Jumper brand resources and styleguide">
        <meta name="og:title" content="Jumper Brand Resources">
        <meta name="og:description" content="Jumper brand resources and styleguide">  
        <meta itemprop="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="twitter:image:src" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">
        <meta name="og:image" content="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/jumper-meta.png">            
    </head>
    <body class="section-padding-lg">
    	<form id="form1" runat="server">
            <uc:overlay id="overlay1" runat="server"></uc:overlay>
            <uc:banner id="banner1" runat="server"></uc:banner>
            <uc:header id="header1" runat="server"></uc:header>
            
            <div class="section-atf section-gradient">
                <div class="container">
                    <div class="row row-cover row-atf row-flex row-flex-center">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <h1>Jumper Brand Resources</h1>
                                <h2>Learn about the Jumper brand, find resources to attract new customers, and build a better experience for your existing ones.</h2>
                            </div>
                            <div class="component component-v-spacing align-center-buttons text-center">
                                <a href="#BrandSharing" class="cta cta-main cta-white">Share Buttons</a>
                                <a href="#BrandLogos" class="cta cta-main cta-white">Logos</a>
                                <a href="#BrandColors" class="cta cta-main cta-white">Colors</a>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div id="BrandSharing" class="section-white-pure">
                <div class="container">
                    <div class="row row-cover row-flex">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <h3>Share Buttons</h3>
                                <h4>Encourage your customers to download Jumper and start video calls with your team. Our branding works best when it's consistent across all mediums - please use the below assets for your embeds, and sharing needs.</h4>
                            </div>
                            <div class="row">
                                <div class="component component-v-spacing component-brand-sharing text-center">
                                    <div class="col-md-12">
                                        <link rel="stylesheet" type="text/css" href="https://static.hellojumper.com/css/sharing.css">
                                        <a class="jumper-component-sharing-link" href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488" target="blank">
                                            <img class="jumper-component-sharing-image" src="https://static.hellojumper.com/img/embed-1.png" />
                                        </a>
                                        <h5>Option 1 Code:</h5>
<pre>
&lt;link rel="stylesheet" type="text/css" href="https://static.hellojumper.com/css/sharing.css"&gt;
&lt;a class="jumper-component-sharing-link" href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488" target="blank"&gt;
    &lt;img class="jumper-component-sharing-image" src="https://static.hellojumper.com/img/embed-1.png" /&gt;
&lt;/a&gt;
</pre>
                                    </div>
                                    <div class="col-md-12">
                                        <link rel="stylesheet" type="text/css" href="https://static.hellojumper.com/css/sharing.css">
                                        <a class="jumper-component-sharing-link" href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488" target="blank">
                                            <img class="jumper-component-sharing-image" src="https://static.hellojumper.com/img/embed-2.png" />
                                        </a>
                                        <h5>Option 2 Code:</h5>
<pre>
&lt;link rel="stylesheet" type="text/css" href="https://static.hellojumper.com/css/sharing.css"&gt;
&lt;a class="jumper-component-sharing-link" href="https://itunes.apple.com/us/app/jumper-video-call-businesses/id1418965488" target="blank"&gt;
    &lt;img class="jumper-component-sharing-image" src="https://static.hellojumper.com/img/embed-2.png" /&gt;
&lt;/a&gt;
</pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BrandLogos" class="section-white">
                <div class="container">
                    <div class="row row-cover row-flex">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <h3>Logos</h3>
                                <h4>The Jumper logo is comprised of two map pins (representing a customer, and a business), which gracefully overlap - nearing the shape of a heart, and symbolizing our mission: to bring people closer to the places they want to go.</h4>
                            </div>
                            <div class="row">
                                <div class="component component-v-spacing component-brand-logos text-center">
                                    <div class="col-md-4">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/logo-customer-new.png">
                                        <h5>Jumper Logo</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/logo-partner-new.png">
                                        <h5>Jumper Partner Logo</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["StaticPath"] %>/img/logo-transparent-new.png">
                                        <h5>Transparent Logo</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="BrandColors" class="section-white-pure">
                <div class="container">
                    <div class="row row-cover row-flex">
                        <div class="col-md-12">
                            <div class="component text-center">
                                <h3>Colors</h3>
                                <h4>The Jumper colors embody movement, excitement, and warmth - meeting a stranger face-to-face can seem cold, and unknown. Jumper makes it anything but that.</h4>
                            </div>
                            <div class="row">
                                <div class="component component-v-spacing component-brand-colors text-center">
                                    <div class="col-md-6">
                                        <div class="brand-color brand-color-primary"><h5>#E7683F</h5></div>
                                        <h5>Primary Color</h5>
                                        <h6>Themed throughout our platform</h6>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="brand-color brand-color-accent"><h5>#E74D56</h5></div>
                                        <h5>Accent Color</h5>
                                        <h6>Used for highlighting features, and within subtle gradients</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- for homepage footer -->

            <uc:footer id="footer1" runat="server"></uc:footer>

            <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
            <script>
                (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                e.src='//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
                ga('create','UA-XXXXX-X','auto');ga('send','pageview');
            </script>            
        </form>
    </body>
</html>
